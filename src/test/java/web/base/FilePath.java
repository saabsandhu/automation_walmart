package web.base;

public interface FilePath {
	// Path for Config.properties
    String CONFIGPath=System.getProperty("user.dir")+"/src/main/resources/config/CONFIG.properties";

  	// Path for Web Drivers
    String ChromeDriverPath=System.getProperty("user.dir")+"/src/main/resources/drivers/chromedriver.exe";
    String FirefoxDriverPath=System.getProperty("user.dir")+"/src/main/resources/drivers/geckodriver.exe";

    // Path for log file
    String LogFilePath=System.getProperty("user.dir")+"/src/main/resources/config/log4j.properties";

}
