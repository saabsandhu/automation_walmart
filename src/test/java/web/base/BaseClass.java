
package web.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class BaseClass implements FilePath {
	
	protected static Properties CONFIG = null;

	public static Logger Log = Logger.getLogger(BaseClass.class);
	static{
		PropertyConfigurator.configure(FilePath.LogFilePath);
	}


	public BaseClass(){
		CONFIG =new Properties();
		File fileConfig=new File(FilePath.CONFIGPath);
		try {
			FileReader frConfig=new FileReader(fileConfig);
			try {
				CONFIG.load(frConfig);
			} catch (IOException e) {
				// TODO Auto-generated catch block

				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
