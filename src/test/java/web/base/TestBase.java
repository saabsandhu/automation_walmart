package web.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import web.utils.Selenium;

public class TestBase extends BaseClass implements FilePath {

	public String browser;
	public WebDriver driver;
	public Selenium selenium;

	@BeforeTest
	public void setUp() throws Exception {
		browser = CONFIG.getProperty("browserType");
		if(browser.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", FilePath.ChromeDriverPath);
			driver = new ChromeDriver();
		} else
		if(browser.equals("firefox")){
			System.setProperty("webdriver.gecko.driver", FilePath.FirefoxDriverPath);
			driver = new FirefoxDriver();
		}

		driver.get(CONFIG.getProperty("initURL"));
		driver.manage().window().maximize();
		selenium = new Selenium(driver);
	}

	@AfterTest
	public void tearDown() throws Exception {
		if (driver!=null)
			driver.quit();
	}

}


