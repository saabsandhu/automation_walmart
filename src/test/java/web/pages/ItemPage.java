package web.pages;

import web.utils.Selenium;

public class ItemPage {
    private Selenium selenium;

    private String productImageXPath = ".//ul[@class='slides large']//li[@class='flex-active-slide']//div[@class='centered-img']//div[@class='centered-img-wrap']//img";
    private String productNameXPath = ".//h1[@data-analytics-type='productPage-productName']";
    private String productDescriptionXPath = ".//p[@class='description']";
    private String brandXPath = ".//p[@class='brand']";
    private String brandLinkXPath = ".//a[@class='brand-link']";
    private String sellerInfoXPath =".//p[@class='seller-info']";
    private String qtyXPath = ".//input[@id='qty-input-productQty-10320406']";
    private String priceXPath = ".//div[@class='price-current']//div[1]";
    private String moreBtnProductQtyXPath = ".//button[@id='more-btn-productQty-10320406']";
    private String lessBtnProductQtyXPath = ".//button[@id='less-btn-productQty-10320406']";
    private String productReturnLinkBtnXPath = ".//button[@class='product-return-btn link-button msg']";

    private String productBreadcrumbXPath = ".//span[@itemprop='name'][contains(text(),'Intex Metal Frame Pool')]";


    public ItemPage(Selenium selenium) {
        this.selenium = selenium;
    }

    public boolean isProductImageDisplayed() {
        return selenium.checkElementVisible(productImageXPath);
    }

    public boolean isProductNameDisplayed() {
        return selenium.checkElementVisible(productNameXPath);
    }

    public boolean isProductDescriptionDisplayed() {
        return selenium.checkElementVisible(productDescriptionXPath);
    }

    public boolean isBrandDisplayed() {
        return selenium.checkElementVisible(brandXPath);
    }

    public boolean isBrandLinkDisplayed() {
        return selenium.checkElementVisible(brandLinkXPath);
    }

    public boolean isSellerInfoDisplayed() {
        return selenium.checkElementVisible(sellerInfoXPath);
    }

    public boolean isQuantityDisplayed() {
        return selenium.checkElementVisible(qtyXPath);
    }

    public boolean isPriceDisplayed() {
        return selenium.checkElementVisible(priceXPath);
    }

    public boolean isMoreBtnProductQuantityDisplayed() {
        return selenium.checkElementVisible(moreBtnProductQtyXPath);
    }
    public boolean isLessBtnProductQuantityDisplayed() {
        return selenium.checkElementVisible(lessBtnProductQtyXPath);
    }

    public boolean isProductReturnLinkBtnDisplayed() {
        return selenium.checkElementVisible(productReturnLinkBtnXPath);
    }

    public boolean isProductNameInBreadcrumbDisplayed() {
        return selenium.checkElementVisible(productBreadcrumbXPath);
    }

    public void verifyProductNameInBreadcrumb(String str){
        selenium.verifyTextContains(productBreadcrumbXPath, str);
    }

}
