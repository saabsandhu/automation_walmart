package web.pages;

import web.utils.Selenium;

public class HomePage {

    private Selenium selenium;

    private String walmartLogoXPath = ".//div[@class='wgrid-2of12 logo-wrap desktop-only']//a//img[@id='logo']";
    private String searchInputXPath = ".//input[@id='global-search']";
    private String searchBtnXPath = ".//form[@class='search-form']//button[@type='submit']";
    private String searchResultXPath = ".//div[@class='results search-lvl1']";



    public HomePage(Selenium selenium) {
        this.selenium = selenium;
    }

    public boolean isWalmartLogoDisplayed(){
        return selenium.checkElementVisible(walmartLogoXPath);
    }

    public boolean isSearchBarDisplayed(){
        return selenium.checkElementVisible(searchInputXPath);
    }

    public void clickWalmartLogo(){
        selenium.clickWithoutScroll(walmartLogoXPath);
        selenium.waitForJQuery();
    }

    public void verifyUrl(String url){
        selenium.verifyUrl(url);
    }

    public void searchPicture(String pictureStr){
        selenium.waitForJQuery();
        selenium.sendKeys(searchInputXPath, pictureStr);
        selenium.click(searchBtnXPath);
    }

    public void verifySearchResult(String str){
        selenium.verifyTextContains(searchResultXPath, str);
    }



}
