package web.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import web.base.TestBase;
import web.pages.ItemPage;

public class testProductDetails extends TestBase {

	@BeforeTest
	public void initialize() {

	}

	@Test
	public void test() throws Exception {
		ItemPage itemPage = new ItemPage(selenium);

		Assert.assertTrue(itemPage.isProductImageDisplayed());
		Assert.assertTrue(itemPage.isProductNameDisplayed());
		Assert.assertTrue(itemPage.isProductDescriptionDisplayed());
		Assert.assertTrue(itemPage.isBrandDisplayed());
		Assert.assertTrue(itemPage.isBrandLinkDisplayed());
		Assert.assertTrue(itemPage.isSellerInfoDisplayed());
		Assert.assertTrue(itemPage.isQuantityDisplayed());
		Assert.assertTrue(itemPage.isLessBtnProductQuantityDisplayed());
		Assert.assertTrue(itemPage.isMoreBtnProductQuantityDisplayed());
		Assert.assertTrue(itemPage.isPriceDisplayed());
		Assert.assertTrue(itemPage.isProductReturnLinkBtnDisplayed());
	}
}
