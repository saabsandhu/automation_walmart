package web.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import web.base.TestBase;
import web.pages.HomePage;
import web.pages.ItemPage;

public class testWalmartLogo extends TestBase {
    String strUrl;

	@BeforeTest
	public void initialize() {
		strUrl = "https://www.walmart.ca/en";
	}

	@Test
	public void test() throws Exception {
		HomePage homePage = new HomePage(selenium);

		Assert.assertTrue(homePage.isWalmartLogoDisplayed());
		homePage.clickWalmartLogo();
		homePage.verifyUrl(strUrl);

	}
}
