package web.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import web.base.TestBase;
import web.pages.ItemPage;

public class testProductNameInBreadcrumb extends TestBase {
	String productNameStr;
	@BeforeTest
	public void initialize() {
		productNameStr = "Intex Metal Frame Pool";
	}

	@Test
	public void test() throws Exception {
		ItemPage itemPage = new ItemPage(selenium);

		Assert.assertTrue(itemPage.isProductNameInBreadcrumbDisplayed());
		itemPage.verifyProductNameInBreadcrumb(productNameStr);

	}
}
