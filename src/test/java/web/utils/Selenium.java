package web.utils;

import org.apache.log4j.Logger;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import web.base.BaseClass;

public class Selenium extends BaseClass {
    public static Logger log = Logger.getLogger(BaseClass.class);
    protected final static int secondsToTry = 30; // Timeout in seconds
    private final static int secondsToCheck = 5; // Timeout used for check methods
    protected final static int clickDelay = 150; // Delay after clicking in ms
    private final static int jQueryTimeout = 45; // Time to wait for jQuery before timing out in seconds
    public WebDriver driver;
    protected WebDriverWait wdw;
    protected Actions actions;
    private boolean clickScrolling = true;

    /**
     * Selenium Class Default Constructor
     *
     */
    public Selenium() {	}

    /**
     * Selenium Class Constructor
     *
     * @param newDriver Selenium Web Driver to be used by this class
     */
    public Selenium(WebDriver newDriver) {
        driver = newDriver;
        wdw = new WebDriverWait(driver, secondsToTry);
        actions = new Actions(driver);
    }

    /**
     *  Used to toggle automatic scrolling during clicks
     * @param clickScrolling If set to True, selenium will use JS to scroll to elements
     */
    public void setClickScrolling(boolean clickScrolling) {
        this.clickScrolling = clickScrolling;
    }

    /**
     * Sets up the browser to the correct URL to use,
     *
     * @param url URL to access on browser set up
     */
    public void goToUrl(String url) {
        log.debug("Selenium.goToUrl: " + url);
        driver.get(url);
    }

    /**
     * Returns the current url.
     *
     * @return current url
     */
    public String getUrl() {
        log.debug("Selenium.getUrl returning current url");
        return driver.getCurrentUrl();
    }

    /**
     * Verifies that the current url is the same as the expected url
     * Fails the test if action could not be completed.
     *
     * @param expectedUrl to check against
     */
    public void verifyUrl(String expectedUrl) {
        log.debug("Selenium.verifyUrl(expectedUrl): " + expectedUrl);
        try {
            wdw.until(ExpectedConditions.urlToBe(expectedUrl));
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifies that the current url contains the expected string
     * Fails the test if action could not be completed, or condition not met.
     *
     * @param expectedUrl string to check
     */
    public void verifyUrlContains(String expectedUrl) {
        log.debug("Selenium.verifyUrlContains(expectedUrl): " + expectedUrl);
        try {
            wdw.until(ExpectedConditions.urlContains(expectedUrl));
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Scroll the given element into view using javascript.
     *
     * @param by By locator of element to scroll to
     */
    private void scrollIntoView(By by) {
        log.debug("Selenium.scrollIntoView(element): " + by.toString());
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(by));
        } catch (StaleElementReferenceException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(by));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Scroll the given element into view using javascript.
     *
     * @param xpath xpath of the element to scroll to
     */
    public void scrollIntoView(String xpath) {
        log.debug("Selenium.scrollIntoView(element): " + xpath);
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(xpath)));
        } catch (StaleElementReferenceException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(xpath)));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clicks element passed.  Uses descriptor for logging purposes.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     *
     * @param by By locator type to click
     */
    protected void click(final By by) {
        log.debug("Selenium.click(by): " + by.toString());
        try {
            WebElement element = getWebElement(by);
            wdw.until(ExpectedConditions.elementToBeClickable(element));
            if (clickScrolling)	scrollIntoView(by); // Scroll to the element
            actions.click(element); // Centre mouse on element then click
            actions.perform();
        } catch (StaleElementReferenceException e) { //retries the click after acquiring the element again
            log.warn("First click errored, retrying");
            WebElement element = getWebElement(by);
            wdw.until(ExpectedConditions.elementToBeClickable(element));
            if (clickScrolling)	scrollIntoView(by); // Scroll to the element
            actions.click(element); // Centre mouse on element then click
            actions.perform();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        pause(clickDelay);
    }

    /**
     * Same as click method, but disables scrolling
     * @param by By locator type to click
     */
    private void clickWithoutScroll(By by){
        boolean originalScroll = clickScrolling;
        clickScrolling = false;
        click(by);
        clickScrolling = originalScroll;
    }

    public void clickWithoutScroll(String xpath){
        clickWithoutScroll(By.xpath(xpath));
    }

    /**
     * Clicks the specified xpath.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to click
     */
    public void click(String xpath) {
        log.debug("Selenium.click(xpath) :" + xpath);
        click(By.xpath(xpath));
    }

    /**
     * Pauses test for specified milliseconds.
     *
     * @param milliseconds to sleep
     */
    public void pause(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            log.error("Sleep interrupted: " + e.getMessage());
        }
    }

    /**
     * Use when you need to wait for async JS calls to complete, or to see if page loaded.
     * Method will try for {@link #secondsToTry} before failing the test .
     */
    public void waitForJQuery() {
        waitForJQuery(jQueryTimeout);
    }

    /**
     * Use when you need to wait for async JS calls to complete, or to see if page loaded.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param timeout to try in milliseconds
     */
    public void waitForJQuery(int timeout) {
        try {
            log.debug("Selenium.waitForJQuery: " + timeout);
            (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver d) {
                    JavascriptExecutor js = (JavascriptExecutor) d;
                    return (Boolean)
                            js.executeScript("return !!window.jQuery &&" +
                                    " window.jQuery.active == 0 &&" +
                                    " document.readyState == \"complete\"");
                }
            });
        } catch (TimeoutException e) {
            log.error("waitForJQuery has experienced a TimeoutException: " + e.getMessage());
        } catch (WebDriverException e) {
            log.error("waitForJQuery has experienced a WebDriverException: " + e.getMessage());
        }
    }



    /**
     * Verifies if the specified xpath exists on the current page.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     * @return <code>true</code> if xpath present, fails test if xpath is not present
     */
    public boolean verifyElementPresent(String xpath) {
        return verifyElementPresent(By.xpath(xpath));
    }

    /**
     * Verifies if the specified locator exists on the current page.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by By locator to verify
     * @return <code>true</code> if by locator present, fails test if by locator is not present
     */
    private boolean verifyElementPresent(By by) {
        return getWebElement(by) != null;
    }

    /**
     * Verifies if the specified xpath does not exist on the current page.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     */
    public void verifyElementNotPresent(final String xpath) {
        log.debug("Selenium.verifyElementNotPresent(xpath): " + xpath);
        try {
            wdw.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver webDriver) {
                    return driver.findElements(By.xpath(xpath)).size() == 0;
                }
            });
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the specified xpath exists on the current page, returns true or false (does not Assert, will not fail the test.)
     *
     * @param xpath xpath to verify
     * @return <code>true</code> if xpath present, <code>false</code> if xpath is not present
     */
    public boolean checkElementPresent(String xpath) {
        return driver.findElements(By.xpath(xpath)).size() > 0;
    }

    /**
     * Verifies if the specified By locator is visible on the current page.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by by locator to verify
     */
    public void verifyElementVisible(By by) {
        log.debug("Selenium.verifyElementVisible(by): " + by.toString());
        try {
            wdw.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifies if the specified xpath is visible on the current page.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     */
    public void verifyElementVisible(String xpath) {
        verifyElementVisible(By.xpath(xpath));
    }

    /**
     * Checks if the specified xpath is visible on the current page, no exception thrown / will not fail the test
     *
     * @param xpath xpath to verify
     * @return <code>true</code> if xpath present, <code>false</code> if xpath is not present
     */
    public boolean checkElementVisible(String xpath) {
        log.debug("Selenium.checkElementVisible(xpath): " + xpath);
        WebDriverWait shortWait = new WebDriverWait(driver, secondsToCheck);
        try {
            shortWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            return true;
        } catch (TimeoutException e) {
            return false;
        } catch (NoSuchElementException e) {
            log.warn("checkElementVisible: Element did not exist " + xpath);
        }
        return false;
    }

    /**
     * Simulates typing keys into the specified xpath.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by   By locator of  field to send keys to
     * @param keys keys to send to xpath
     */
    private void sendKeys(By by, String keys) {
        log.debug("Selenium.sendKeys(by, keys): " + by.toString() + ", " + keys);
        try {
            wdw.until(ExpectedConditions.visibilityOf(driver.findElement(by)));
            driver.findElement(by).sendKeys(keys);
        } catch (StaleElementReferenceException e) {
            driver.findElement(by).sendKeys(keys);
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Simulates typing keys into the specified xpath
     * Method will try for {@link #secondsToTry} before failing the test
     *
     * @param xpath xpath of text field to send keys to
     * @param keys  keys to send to xpath
     */
    public void sendKeys(String xpath, String keys) {
        sendKeys(By.xpath(xpath), keys);
    }

    /**
     * Simulates typing keys into the specified xpath using the Keys class
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath of text field to send keys to
     * @param key   keys to send to xpath
     */
    public void sendKeys(String xpath, Keys key) {
        sendKeys(xpath, key.toString());
    }

    /**
     * Clears the content of the passed element.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by By locator to clear
     */
    private void clear(By by) {
        log.debug("Selenium.clear(by): " + by.toString());
        try {
            wdw.until(ExpectedConditions.visibilityOf(driver.findElement(by)));
            driver.findElement(by).clear();
        } catch (StaleElementReferenceException e) {
            driver.findElement(by).clear();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Simulates clearing a text field at the specified xpath
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath of text field to clear
     */
    public void clear(String xpath) {
        clear(By.xpath(xpath));
    }

    /**
     * Verifies text at a specified xpath
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     * @param text  expected value
     * @return true if text at specified xpath matches expected text, fails the test otherwise
     */
    public void verifyText(String xpath, String text) {
        log.debug("Selenium.verifyText(xpath, text): " + xpath + ", " + text);
        verifyElementPresent(xpath);
        try {
            wdw.until(ExpectedConditions.textToBe(By.xpath(xpath), text));
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifies that text exists at a specified xpath ignoring casing
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     * @param text string to verify against
     * @return <code>true</code> if text at specified xpath matches, fails the test otherwise.
     */
    public boolean verifyTextIgnoreCase(final String xpath, final String text) {
        log.debug("Selenium.verifyTextIgnoreCase(xpath, String): " + xpath + ", " + text);
        verifyElementPresent(xpath);
        try {
            wdw.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver d) {
                    return driver.findElement(By.xpath(xpath)).getText().toLowerCase().equals(text.toLowerCase());
                }
            });
            return true;
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Verifies if text at a specified By locator contains the provided text
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by   By locator to verify
     * @param text expected value
     * @return <code>true</code> if text at specified xpath matches expected text, fails the test if text does not match expected text.
     */
    private boolean verifyTextContains(By by, String text) {
        log.debug("Selenium.verifyTextContains(by, text): " + by.toString() + ", " + text);
        verifyElementVisible(by);
        try {
            wdw.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
            return true;
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Verifies if text at a specified xpath contains the provided text
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     * @param text  expected value
     */
    public void verifyTextContains(String xpath, String text) {
        verifyTextContains(By.xpath(xpath), text);
    }

    /**
     * Verifies that text at a specified By locator does not contain the provided text
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by   By locator to verify
     * @param text expected value
     * @return <code>true</code> if text at specified xpath matches expected text, fails test if text does not match expected text.
     */
    private boolean verifyTextNotContains(By by, String text) {
        log.debug("Selenium.verifyTextNotContains(by, text): " + by.toString() + ", " + text);
        verifyElementPresent(by);
        try {
            wdw.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(by, text)));
            return true;
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Verifies that text at a specified xpath does not contain the provided text
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to verify
     * @param text  expected value
     * @return <code>true</code> if text at specified xpath matches expected text, fails test if text does not match expected text.
     */
    public boolean verifyTextNotContains(String xpath, String text) {
        return verifyTextNotContains(By.xpath(xpath), text);
    }

    /**
     * Checks if the passed XPath contains expected test.  If expected text is not contained the test will not fail.
     *
     * @param xpath to check
     * @param text  expected text
     * @return true if xpath contains expected text, false otherwise
     */
    public boolean checkTextContains(final String xpath, final String text) {
        log.debug("Selenium.checkTextContains(xpath, text): " + xpath + ", " + text);
        WebDriverWait shortWait = new WebDriverWait(driver, secondsToCheck);
        try {
            shortWait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(xpath), text));
            return true;
        } catch (TimeoutException e) {
            return false;
        } catch (NoSuchElementException e) {
            log.warn("Could not find xpath " + xpath);
        }
        return false;
    }

    /**
     * Returns the text contained within the node at a specified xpath
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath of the text to be returned
     * @return value at the specified xpath
     */
    public String getText(String xpath) {
        log.debug("Selenium.getText(xpath): " + xpath);
        try {
            WebElement element = getWebElementByXpath(xpath);
            verifyElementVisible(xpath);
            return element.getText();
        } catch (StaleElementReferenceException e) {
            WebElement element = getWebElementByXpath(xpath);
            return element.getText();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Returns the web element at the provided By locator.
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param by By locator of element
     * @return webElement at location
     */
    protected WebElement getWebElement(By by) {
        log.debug("Selenium.getWebElement(by): " + by.toString());
        try {
            wdw.until(ExpectedConditions.presenceOfElementLocated(by));
            return driver.findElement(by);
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Method that return a WebElement based on provided xpath
     * Method will try for {@link #secondsToTry} before failing the test .
     *
     * @param xpath xpath to use to find element
     */
    public WebElement getWebElementByXpath(String xpath) {
        return getWebElement(By.xpath(xpath));
    }

}

